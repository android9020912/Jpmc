package com.jpmc.nycschool.di

import com.jpmc.nycschool.data.service.SchoolService
import com.jpmc.nycschool.data.repository.SchoolRepository
import com.jpmc.nycschool.domain.repository.ISchoolRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    fun provideAnalyticsService(): SchoolService {
        return Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/resource/")
            .client(OkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SchoolService::class.java)
    }

    @Provides
    fun provideSchoolRepository(service: SchoolService): ISchoolRepository {
        return SchoolRepository(service)
    }
}
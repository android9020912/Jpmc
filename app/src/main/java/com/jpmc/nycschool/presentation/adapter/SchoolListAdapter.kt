package com.jpmc.nycschool.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.databinding.ListItemBinding

class SchoolListAdapter(private val onClick: (SchoolDataItem) -> Unit) :
    ListAdapter<SchoolDataItem, SchoolListAdapter.SchoolListViewHolder>(SchoolDiffCallback) {

    class SchoolListViewHolder(
        itemView: ListItemBinding,
        val onClick: (SchoolDataItem) -> Unit
    ) : RecyclerView.ViewHolder(itemView.root) {

        private val schoolName: TextView = itemView.schoolName
        private val schoolCity: TextView = itemView.schoolCity
        private var currentSchool: SchoolDataItem? = null

        init {
            itemView.root.setOnClickListener {
                currentSchool?.let {
                    onClick(it)
                }
            }
        }

        /* Bind School Name. */
        fun bind(school: SchoolDataItem) {
            currentSchool = school
            schoolName.text = school.schoolName
            schoolCity.text = school.schoolCity
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolListViewHolder {
        val view = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolListViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: SchoolListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

object SchoolDiffCallback : DiffUtil.ItemCallback<SchoolDataItem>() {
    override fun areItemsTheSame(
        oldItem: SchoolDataItem,
        newItem: SchoolDataItem
    ): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: SchoolDataItem,
        newItem: SchoolDataItem
    ): Boolean {
        return oldItem.schoolDbn == newItem.schoolDbn
    }

}
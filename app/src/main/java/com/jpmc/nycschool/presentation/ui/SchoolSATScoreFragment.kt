package com.jpmc.nycschool.presentation.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.jpmc.nycschool.R
import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.databinding.FragmentSchoolSatScoreBinding
import com.jpmc.nycschool.presentation.viewmodel.SchoolSATScoreViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SchoolSATScoreFragment : Fragment() {

    private var schoolDBN: SchoolDataItem? = null
    private var _binding: FragmentSchoolSatScoreBinding? = null
    private val viewModel by viewModels<SchoolSATScoreViewModel>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                schoolDBN = it.getParcelable("schoolArg",SchoolDataItem::class.java)
            } else {
                schoolDBN = it.getParcelable("schoolArg")
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSchoolSatScoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvSchoolName.text = schoolDBN?.schoolName
        binding.tvSchoolOverview.text = schoolDBN?.schoolOverview
        binding.tvWebsite.text = schoolDBN?.schoolWebsite
        binding.tvEmail.text = schoolDBN?.schoolEmail
        binding.tvPhoneNo.text = schoolDBN?.schoolPhoneNumber

        viewModel.liveData.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = View.GONE
            binding.schoolTestTakers.text =
                getString(R.string.number_of_students, it.num_of_sat_test_takers)
            binding.schoolMathScore.text =
                getString(R.string.average_maths_score, it.sat_math_avg_score)
            binding.schoolReadingScore.text =
                getString(R.string.average_reading_score, it.sat_critical_reading_avg_score)
            binding.schoolWritingScore.text =
                getString(R.string.average_writing_score, it.sat_writing_avg_score)

        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            if (it) {
                binding.layoutChild.visibility = View.GONE
                Toast.makeText(requireContext(), "Could not find SAT Score", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        schoolDBN?.let { viewModel.getSATScore(it.schoolDbn) }
    }

}
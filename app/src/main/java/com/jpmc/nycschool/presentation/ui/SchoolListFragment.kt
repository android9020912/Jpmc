package com.jpmc.nycschool.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.databinding.FragmentFirstBinding
import com.jpmc.nycschool.presentation.adapter.SchoolListAdapter
import com.jpmc.nycschool.presentation.viewmodel.SchoolListViewModel
import dagger.hilt.android.AndroidEntryPoint


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class SchoolListFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val viewModel by viewModels<SchoolListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView: RecyclerView = binding.schoolListRecyclerview
        val layoutManager = LinearLayoutManager(requireContext())
        recyclerView.layoutManager = layoutManager
        val schoolListAdapter = SchoolListAdapter { item -> adapterOnClick(item) }
        recyclerView.adapter = schoolListAdapter

        viewModel.liveData.observe(viewLifecycleOwner) {
            //I would prefer to use Shimmer effect view instead progress bar
            binding.progressBar.visibility = View.GONE
            schoolListAdapter.submitList(it as MutableList<SchoolDataItem>)
        }
        viewModel.getSchools()

    }

    private fun adapterOnClick(schoolItem: SchoolDataItem) {
        val args = Bundle()
        args.putParcelable("schoolArg", schoolItem)
        val action = SchoolListFragmentDirections.navDetailFragment(schoolItem)
        view?.findNavController()?.navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
package com.jpmc.nycschool.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.nycschool.data.model.SchoolSatScoreResponse
import com.jpmc.nycschool.domain.usecase.GetSchoolSatInfoUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolSATScoreViewModel @Inject constructor(
    private val schoolSATUseCase: GetSchoolSatInfoUseCase
) :
    ViewModel() {
    private val _data = MutableLiveData<SchoolSatScoreResponse>()
    val liveData: LiveData<SchoolSatScoreResponse> = _data
    val errorLiveData = MutableLiveData<Boolean>(false)
    fun getSATScore(dbn: String) {
        viewModelScope.launch {
            //here i would prefer to use sealed class for different response like Success, Progress, Error etc to manage all the scenario
            try {
                val selectedItemDetails = schoolSATUseCase.invoke(dbn)
                _data.postValue(selectedItemDetails[0])
                errorLiveData.postValue(false)
            } catch (e: Exception) {
                println(e.localizedMessage)
                errorLiveData.postValue(true)
            }
        }
    }
}
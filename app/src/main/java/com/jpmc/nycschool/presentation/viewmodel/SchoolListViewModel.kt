package com.jpmc.nycschool.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.domain.usecase.GetNycSchoolUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject



@HiltViewModel
class SchoolListViewModel @Inject constructor(
    private val schoolUseCase: GetNycSchoolUseCase) : ViewModel() {
    private val _data = MutableLiveData<ArrayList<SchoolDataItem>>()
    val liveData: LiveData<ArrayList<SchoolDataItem>> = _data
    val errorLiveData = MutableLiveData("")

    fun getSchools() {
        viewModelScope.launch {
            try {
                //Here i would prefer to use sealed class for different response like Success, Progress,
                //Error etc to manage all the scenario
                _data.postValue(schoolUseCase.invoke(null))
            } catch (e: Exception) {
                println(e.localizedMessage)
                errorLiveData.postValue("Something went wrong")
            }
        }

    }
}
package com.jpmc.nycschool.data.service

import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.data.model.SchoolSatScoreResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolService {

    @GET("s3k6-pzi2.json")
    suspend fun getSchoolInfo(): ArrayList<SchoolDataItem>

    @GET("f9bf-2cp4.json")
    suspend fun getSATInfoFromSchool(
        @Query("dbn", encoded = true) cityName: String,
    ): ArrayList<SchoolSatScoreResponse>
}
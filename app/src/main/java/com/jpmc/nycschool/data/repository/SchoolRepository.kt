package com.jpmc.nycschool.data.repository

import com.jpmc.nycschool.data.service.SchoolService
import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.data.model.SchoolSatScoreResponse
import com.jpmc.nycschool.domain.repository.ISchoolRepository
import javax.inject.Inject

class SchoolRepository @Inject constructor(private val schoolService: SchoolService) :
    ISchoolRepository {

    override suspend fun getSchools(): ArrayList<SchoolDataItem> {
        return schoolService.getSchoolInfo()
    }

    override suspend fun getSATInfoFromSchool(dbn: String): ArrayList<SchoolSatScoreResponse> {
        return schoolService.getSATInfoFromSchool(dbn)
    }
}
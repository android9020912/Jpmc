package com.jpmc.nycschool.presentation.viewmodel


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jpmc.nycschool.MainCoroutineScopeRule
import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.domain.usecase.GetNycSchoolUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class SchoolListViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainTaskExecutorRule = MainCoroutineScopeRule()

    @Mock
    lateinit var schoolUseCase: GetNycSchoolUseCase

    private val testDispatcher = TestCoroutineDispatcher()
    private val nycSchoolsResponseItem: SchoolDataItem = Mockito.mock()

    private lateinit var viewModel: SchoolListViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        viewModel = SchoolListViewModel(
            schoolUseCase
        )
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `test getSchools() success`() = testDispatcher.runBlockingTest {
        // Mock data and behavior for successful school retrieval
        val mockSchools = arrayListOf(nycSchoolsResponseItem, nycSchoolsResponseItem)
        Mockito.`when`(schoolUseCase.invoke(null)).thenReturn(mockSchools)

        // Call the function to be tested
        viewModel.getSchools()

        // Advance the coroutine dispatcher to execute any coroutines launched inside the function
        testDispatcher.scheduler.advanceUntilIdle()

        // Verify that the schoolInfoLiveData contains the expected data
        Assert.assertEquals(mockSchools,viewModel.liveData.value)
        Assert.assertEquals("",viewModel.errorLiveData.value)
    }

    @Test
    fun `test getSchools() error`() = testDispatcher.runBlockingTest {
        // Mock data and behavior for error during school retrieval
        val errorMessage = "Error fetching schools"
        Mockito.`when`(schoolUseCase.invoke(null)).thenThrow(RuntimeException(errorMessage))

        // Call the function to be tested
        viewModel.getSchools()

        // Advance the coroutine dispatcher to execute any coroutines launched inside the function
        testDispatcher.scheduler.advanceUntilIdle()

        // Verify that the errorLiveData contains the expected error message
        Assert.assertEquals(null,viewModel.liveData.value)
        Assert.assertEquals("Something went wrong",viewModel.errorLiveData.value)
    }

}

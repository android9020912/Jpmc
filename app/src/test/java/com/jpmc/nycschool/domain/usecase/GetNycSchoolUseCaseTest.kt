package com.jpmc.nycschool.domain.usecase

import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.data.repository.SchoolRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

internal class GetNycSchoolUseCaseTest {

    @Mock
    lateinit var schoolRepository: SchoolRepository

    private lateinit var getNycSchoolUseCase: GetNycSchoolUseCase
    private val nycSchoolsResponseItem:SchoolDataItem = Mockito.mock()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getNycSchoolUseCase = GetNycSchoolUseCase(schoolRepository)
    }

    @Test
    fun `test invoke() should return list of NYC schools`() = runBlocking {
        // Mock data
        val mockSchoolsList = arrayListOf(
            nycSchoolsResponseItem,
            nycSchoolsResponseItem,
            nycSchoolsResponseItem
        )
        `when`(schoolRepository.getSchools()).thenReturn(mockSchoolsList)

        // Call the function to be tested
        val result = getNycSchoolUseCase.invoke(null)

        // Verify that the result is the same as the mocked data
        assertEquals(mockSchoolsList, result)
    }

}
